# Movi

`git branch -m main master` <br/>
`git fetch origin` <br/>
`git branch -u origin/master master` <br/>
`git remote set-head origin -a` <br/>

# Introduction:

World Movio (Web React Application): A dynamic and fully responsive web app that explores exciting movies and
web shows. Built with React, Axios, Redux, and Custom Hooks, it fetches data in real-time from TMDB’s API, offering an
immersive experience with the help of various useful React libraries.

![image](https://gitlab.com/faiazrahman70/movio/-/raw/master/src/assets/demo2.png?ref_type=heads)

![image](https://gitlab.com/faiazrahman70/movio/-/raw/master/src/assets/demo1.png?ref_type=heads)
